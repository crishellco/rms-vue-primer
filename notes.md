### What is Vue
- Evan You
  - Google Creative Lab, Meteor
- “The Progressive JavaScript Framework”
- Approachable
  - HTML, CSS, JS
- Versatile
  - Progressive
- Performant
  - 20kb
  - Automatic component dependency tracking
- Documentation, community 💯
### Ecosystem
- cli
  - create, serve, ui
- vuex
- vue-router
- vue-test-utils
- ssr
- vue-native
- vev-devtools
### Concepts
- Reactivity
- Virtual DOM
- .Vue components (single file)
- Computed properties
- State (data)
- Props
- Methods
- Events
- Directives
  - Model, loop, conditional
  - Event, prop
- Slots
- Watchers
### Why?
- Challenge and expand knowledge as a team
- Faster onboarding, flat learning curve
- Progressive, lightweight, unopinionated
