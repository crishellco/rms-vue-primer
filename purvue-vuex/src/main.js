import Vue from 'vue';
import Vuex from 'vuex';
import App from './App.vue';

Vue.config.productionTip = false;

Vue.use(Vuex);

const store = new Vuex.Store({
    mutations: {
        addTask(state, name) {
            state.tasks.push({
                completed: false,
                name,
            });
        },

        completeAllTasks(state) {
            state.tasks = state.tasks.map(task => _.set(task, 'completed', true));
        },

        deleteTask(state, task) {
            const index = state.tasks.indexOf(task);

            Vue.delete(state.tasks, index);
        },

        toggleTaskCompleted(state, task) {
            const index = state.tasks.indexOf(task);

            task.completed = !task.completed;

            Vue.set(state.tasks, index, task);
        },
    },

    state: {
        tasks: [
            {
                completed: false,
                name: 'Vue.js Primer Part II @ 12'
            },
        ],
    },
});

new Vue({
    render: h => h(App),
    store,
}).$mount('#app');
