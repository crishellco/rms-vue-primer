#### Running The Basic Demo Locally
```
cd purvue
npm i lodash
npm i -g @vue/cli @vue/cli-service-global
vue serve
```

#### Running The Vuex Demo Locally
```
cd purvue-vuex
npm i
npm run serve
```
